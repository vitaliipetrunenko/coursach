import { Provider } from "react-redux";
import store from "./redux/store";

import Routes from "./routes";
import logo from "./logo.svg";
import "./App.css";
import "antd/dist/antd.css";

function App() {
  return (
    <Provider store={store()}>
      <Routes />
    </Provider>
  );
}

export default App;
