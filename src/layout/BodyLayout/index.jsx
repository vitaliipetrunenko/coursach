import React from "react";
import { Layout, Menu, Breadcrumb } from "antd";
import useStyles from "./useStyles";
import { headerLinks } from "../../constants/links";
import { NavLink } from "react-router-dom";

const { Header, Content, Footer } = Layout;

const BodyLayout = ({ children }) => {
  const classes = useStyles();
  return (
    <>
      <Layout>
        <Header
          style={{
            position: "fixed",
            zIndex: 1,
            width: "100%",
            background:
              "url(https://sites.google.com/site/visivanocka12345/_/rsrc/1429543422232/grupi-ornamentiv/%D0%B3%D0%B5%D0%BE%D0%BC.gif)",
          }}
        >
          <div className="logo" />
          <Menu
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={["2"]}
            items={headerLinks.map((item) => ({
              label: <NavLink to={item.link}>{item.label}</NavLink>,
              key: item.link,
            }))}
            style={{ background: "transparent" }}
            className={classes.menu}
          />
        </Header>
        <Content
          className="site-layout"
          style={{
            padding: "0 50px",
            marginTop: 64,
          }}
        >
          {/* <Breadcrumb
            style={{
              margin: "16px 0",
            }}
          >
            <Breadcrumb.Item>Home</Breadcrumb.Item>
            <Breadcrumb.Item>List</Breadcrumb.Item>
            <Breadcrumb.Item>App</Breadcrumb.Item>
          </Breadcrumb> */}
          <div
            className="site-layout-background"
            style={{
              padding: 24,
              minHeight: 380,
            }}
          >
            {children}
          </div>
        </Content>
        {/* <Footer
          style={{
            textAlign: "center",
            background: "lightgray",
          }}
        >
          footer
        </Footer> */}
      </Layout>
    </>
  );
};

export default BodyLayout;
