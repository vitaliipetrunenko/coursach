import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
  header: {
    background: "blue",
  },
  body: {
    background: "yellow",
  },
  menu: {
    "& .ant-menu-item-selected": {
      background: "linear-gradient(0deg, 	#FFFF99 50%, #1890ff 50%) !important",
    },
    "& .ant-menu-item": {
      "&:hover": {
        background: "linear-gradient(0deg, 	#FFFF99 50%, #1890ff 50%)",
      },
      borderRadius: 5,
      marginRight: 5,
      // background: "linear-gradient(0deg, 	#FFFF99 50%, #0000aa 50%)"
      "& a": {
        background: "white",
        borderRadius: "20px",
        padding: "0px 10px",
        color: "black !important",
        fontWeight: 700,
        verticalAlign: "middle",
      },
    },
  },
});

export default useStyles;
