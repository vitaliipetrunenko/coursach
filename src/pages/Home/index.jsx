import { Button, Carousel } from "antd";
import React from "react";
import { NavLink } from "react-router-dom";
import useStyles from "./useStyles";
const slide1 = "https://uploads5.wikiart.org/00379/images/halyna-dubyk/2.png";
const slide2 =
  "https://img1.goodfon.ru/wallpaper/nbig/3/42/ilya-repin-zaporozhcy-pishut.jpg";
const slide3 =
  "https://galinfo.com.ua/media/gallery/full/9/1/91775898_1374299109416177_1437409804602572800_o.jpg";

const contentStyle = {
  height: "90vh",
  color: "#fff",
  lineHeight: "160px",
  textAlign: "center",
  background: "rgba(0,0,0,0.1)",
  marginBottom: 0,
};
const imgStyle = {
  objectFit: "cover",
  width: "100%",
  height: "100%",
};

const Home = () => {
  const classes = useStyles();
  return (
    <>
      <div className={classes.carouselWrapper}>
        <Carousel autoplay>
          {/* <div>
          <h3 style={contentStyle}>
            {" "}
            <img style={imgStyle} src={slide1} />1
          </h3>
        </div> */}
          <div>
            <h3 style={contentStyle}>
              <img style={imgStyle} src={slide2} />
              <div className={classes.blurCover}>Ukrainian Artist Hub</div>
            </h3>
          </div>
          <div>
            <h3 style={contentStyle}>
              <img style={imgStyle} src={slide3} />
              <div className={classes.blurCover}>Ukrainian Artist Hub</div>
            </h3>
          </div>
        </Carousel>
      </div>
      <NavLink to="/artists">
        <Button style={{ width: "100%" }} type="primary">
          Browse Artists
        </Button>
      </NavLink>
    </>
  );
};

export default Home;
