import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
  header: {
    background: "blue",
  },
  body: {
    background: "yellow",
  },
});

export default useStyles;
